package fari.iitu.ead.dto;

import lombok.Data;

@Data
public class AddNoteRequest {

    private Integer token;

    private String name;

    private String description;
}
