package fari.iitu.ead.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserNotesResponse {

    private String name;
    private String description;
}
