package fari.iitu.ead.repository;


import fari.iitu.ead.entity.Note;
import fari.iitu.ead.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoteRepository extends JpaRepository<Note , Long> {
    List<Note> findAllByUser_Token(int token);
    Note findByIdAndUser_Token(Long id , int token);
}
