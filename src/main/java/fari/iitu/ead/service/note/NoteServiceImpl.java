package fari.iitu.ead.service.note;

import fari.iitu.ead.dto.AddNoteRequest;
import fari.iitu.ead.dto.EditNoteRequest;
import fari.iitu.ead.dto.UserNotesResponse;
import fari.iitu.ead.entity.Note;
import fari.iitu.ead.entity.User;
import fari.iitu.ead.repository.NoteRepository;
import fari.iitu.ead.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
public class NoteServiceImpl implements NoteService{

    private final NoteRepository noteRepository;
    private final UserRepository userRepository;

    @Autowired
    public NoteServiceImpl(NoteRepository noteRepository, UserRepository userRepository) {
        this.noteRepository = noteRepository;
        this.userRepository = userRepository;
    }

    @Override
    public ResponseEntity<?> addNote(AddNoteRequest request) {
        User user = userRepository.findByToken(request.getToken());
        if (user==null){
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    "Incorrect token"
            );
        }
        Note note = new Note();
        note.setName(request.getName());
        note.setDescription(request.getDescription());
        note.setUser(user);
        noteRepository.save(note);
        return new ResponseEntity<>(
                "Created",
                HttpStatus.OK
        );
    }

    @Override
    public List<UserNotesResponse> getAllUserNotes(int token) {
        List<Note> notes = noteRepository.findAllByUser_Token(token);
        List<UserNotesResponse> result = new ArrayList<>();
        for (Note n : notes){
            result.add(new UserNotesResponse(n.getName() , n.getDescription()));
        }
        return result;
    }

    @Override
    public ResponseEntity<?> editNote(int token , Long id , EditNoteRequest request) {
        Note note = noteRepository.findByIdAndUser_Token(id , token);
        System.err.println(id);
        if (note==null){
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    "Does not exist"
            );
        }
        if (request.getName()!=null) {
            note.setName(request.getName());
        }
        if (request.getDescription()!=null) {
            note.setDescription(request.getDescription());
        }
        noteRepository.save(note);
        return new ResponseEntity<>("Edited" , HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> deleteNote(int token, Long id) {
        Note note = noteRepository.findByIdAndUser_Token(id , token);
        if (note==null){
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    "does not exist"
            );
        }
        noteRepository.delete(note);
        return new ResponseEntity<>("Deleted" , HttpStatus.OK);
    }
}
