package fari.iitu.ead.service.user;

import fari.iitu.ead.dto.LoginRequest;
import fari.iitu.ead.dto.RegisterRequest;
import fari.iitu.ead.entity.User;
import fari.iitu.ead.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;
import java.util.Random;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;
    private final Map<String , Object> result;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, Map<String, Object> result) {
        this.userRepository = userRepository;
        this.result = result;
    }


    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public ResponseEntity<?> register(RegisterRequest request) {
        boolean existsByLogin = userRepository.existsByLogin(request.getLogin());
        if (existsByLogin){
            return new ResponseEntity<>("This login already exists" , HttpStatus.CONFLICT);
        }
        User user = new User(request);
        save(user);
        return new ResponseEntity<>("Successfully registered" , HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> login(LoginRequest request) {
        User user = userRepository.findByLogin(request.getLogin());
        if (user==null){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "mot found"
            );
        }
        if (!request.getPassword().equals(user.getPassword())){
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    "Incorrect password"
            );
        }
        Random random = new Random();
        Integer code = random.nextInt((999999 - 100000) + 1) + 100000;

        System.err.println(code);
        user.setToken(code);
        save(user);
        return new ResponseEntity<>("Success, your token is " + code  , HttpStatus.OK
        );
    }
}
