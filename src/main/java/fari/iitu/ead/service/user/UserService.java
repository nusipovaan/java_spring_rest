package fari.iitu.ead.service.user;

import fari.iitu.ead.dto.LoginRequest;
import fari.iitu.ead.dto.RegisterRequest;
import fari.iitu.ead.entity.User;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface UserService {

    void save(User user);

    ResponseEntity<?> register(RegisterRequest request);

    ResponseEntity<?> login(LoginRequest request);
}
